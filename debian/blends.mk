#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2014, 2018  Jonas Smedegaard <dr@jones.dk>
# Description: CDBS template to create blends
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include /usr/share/cdbs/1/rules/buildcore.mk
include /usr/share/cdbs/1/rules/debhelper.mk

#pkgstem = foo
#sharedir = usr/share/$(pkgstem)-blends
#blends = mine yours

# * recent boxer needed for change of skeleton files format
CDBS_BUILD_DEPENDS +=, boxer (>= 0.004~), boxer-data

# resolve release suite
include /usr/share/dpkg/pkg-info.mk

TARGETSUITE ?= $(strip $(or \
 $(if $(filter experimental% UNRELEASED,$(DEB_DISTRIBUTION)),trixie), \
 $(if $(filter stretch% oldstable%,$(DEB_DISTRIBUTION)),bullseye), \
 bookworm))

_pkg-bulk = $(shell perl -0777 -n \
	-e 's/ \\\n / /' \
	-e ' and m,^d-i pkgsel/include string ([^\n]*),ms' \
	-e ' and print $$1' \
	content/$1/preseed.cfg)
pkg-desktop-bulk = $(call _pkg-bulk,desktop)
pkg-desktop-desktop = $(shell perl -n \
	-e 's,^tasksel tasksel/desktop multiselect ,, and print $$_' \
	content/desktop/preseed.cfg)
pkg-desktop-tasks = $(shell perl -n \
	-e 's,^tasksel tasksel/first multiselect ,, and print $$_' \
	content/desktop/preseed.cfg)
pkg-desktop = $(strip \
	$(pkg-desktop-desktop:%=task-%-desktop$(comma)) \
	$(pkg-desktop-tasks:%=task-%$(comma)) \
	$(patsubst %,%$(comma),$(filter-out %-,$(pkg-desktop-bulk))))
pkg-desktop-avoid = $(strip \
	$(patsubst %-,%$(comma),$(filter %-,$(pkg-desktop-bulk))))

define blend_template =
pkg-desktop-$1-bulk = $$(call _pkg-bulk,desktop/$1)
pkg-desktop-$1 = $$(strip \
	$$(filter-out $$(pkg-desktop),\
		$$(patsubst %,%$$(comma),\
			$$(filter-out %-,$$(pkg-desktop-$1-bulk)))))
pkg-desktop-$1-avoid = $$(strip \
	$$(patsubst %-,%$$(comma),\
		$$(filter-out $$(pkg-desktop-bulk),\
			$$(filter %-,$$(pkg-desktop-$1-bulk)))))
CDBS_DEPENDS_$(pkgstem)-desktop-$1 = $$(pkg-desktop-$1)
CDBS_CONFLICTS_$(pkgstem)-desktop-$1-strict = $$(pkg-desktop-$1-avoid)
endef
$(foreach blend,$(blends),$(eval $(call blend_template,$(blend))))

# TODO: merge into main only when per-blend -strict package is missing
pkg-desktop-avoid-all = $(sort $(pkg-desktop-avoid) \
	$(foreach blend,$(blends),$(pkg-desktop-$(blend)-avoid)))
CDBS_DEPENDS_$(pkgstem)-desktop = $(pkg-desktop)
CDBS_CONFLICTS_$(pkgstem)-desktop-strict = $(pkg-desktop-avoid-all)

build/$(pkgstem)-desktop:: content/desktop/preseed.cfg $(pkgstem)-desktop-tidy

content/desktop/preseed.cfg:
	$(MAKE) suite=$(TARGETSUITE)

# check extracted packages and extract tidying script
CDBS_BUILD_DEPENDS +=, shellcheck
$(pkgstem)-desktop-tidy: content/desktop/preseed.cfg
	# check that all package lists are populated
	test -n "$(pkg-desktop)"
	set -e; $(foreach blend,$(blends),test -n "$(pkg-desktop-$(blend))"; )
	perl -0777 -n \
		-e 's,.*d-i preseed/late_command string ,#!/bin/sh\n,ms' \
		-e ' and print $$_' \
		$< > $@
	shellcheck -e SC1003 -e SC2016 -e SC2034 -e SC2046 -e SC2048 -e SC2086 -e SC2129 $@

binary-predeb/$(pkgstem)-data::
	find debian/$(pkgstem)-data/$(sharedir)/data \
		-name script.sh \
		-exec chmod +x {} +

DEB_DH_INSTALL_ARGS_$(pkgstem)-data = content/* $(sharedir)/data
DEB_DH_INSTALL_ARGS_$(pkgstem)-desktop = $(pkgstem)-desktop-tidy usr/sbin

clean::
	$(MAKE) clean
	rm -f $(pkgstem)-desktop-tidy
